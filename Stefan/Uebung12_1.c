#include <stdio.h>
#include <math.h>

int isPrime(int number)
{
	int divider = 2;
	while (divider <= sqrt(number))
	{
		if (number % divider == 0)
		{
			return 0;
		}
		divider++;
	}
	return 1;
}

void printPrimes(int fromNumber, int toNumber) 
{
	int tool = fromNumber;
	//int tool2 = toNumber;
	if (fromNumber > toNumber) 
	{
		printf("Der Anfang soll kleiner als die Ende sein.");
		/*int bucket;
		bucket = tool;
		tool = tool2;
		tool2 = bucket;*/
		//The commented section is to become reality with pointers. TBA
	}
	while (tool < toNumber) {
		if (isPrime(tool)) {
			printf("%d\n", tool);
		}
		tool++;
	}
	
}


int main()
{
	int anfang;
	int ende;
	printf("Geben Sie den Interal ein: ");
	printf("Anfang: ");
	scanf("%d", &anfang);
	printf("Ende: ");
	scanf("%d", &ende);
	printPrimes(anfang, ende);


}