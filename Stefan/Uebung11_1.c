#include <stdio.h>

int lowerToUpper(char c) {
	int res = c;
	if (c >= 'a' && c <= 'z') {
		res = c - 'a' + 'A';
	}
	return res;
}

int main() {

	char str[100];
	scanf("%s", str);
	for (int i2 = 0; i2 < strlen(str); i2++) {
		str[i2] = lowerToUpper(str[i2]);
	}
	printf("%s", str);

	return 0;
}