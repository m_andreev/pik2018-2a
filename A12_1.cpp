#include<stdio.h>
#include<math.h>
int primzahl(int zahl);

int main() {
	int grenze1, grenze2;
	int zahlen;
	printf("Geben sie den Anfang des Intervalls ein: ");
	scanf("%d", &grenze1);
	printf("Geben sie das Ende des Intervalls ein: ");
	scanf("%d", &grenze2);
	printf("Primzahlen sind:\n");
	for (int zahl = grenze1; zahl <= grenze2; zahl++) {
		if (primzahl(zahl)) {
			printf("%d\n", zahl);
		}
		
	}
	
	return 0;
}

int primzahl(int zahl) {
	for (int i = 2; i <= sqrt(zahl); i++) {
		if (zahl%i == 0) {
			return 0;
		}
	}
	return 1;
}
		
